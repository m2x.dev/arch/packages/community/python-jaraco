# Maintainer: Kyle Keen <keenerd@gmail.com>

pkgname=python-jaraco
pkgver=2021.12.20
pkgrel=1
pkgdesc='A gaggle of idiosyncratic and questionable wrappers for the stdlib.'
arch=('any')
url='https://github.com/jaraco?page=1&tab=repositories&utf8=%E2%9C%93&q=jaraco'
license=('MIT')
depends=('python-six' 'python-pytz' 'python-more-itertools' 'python-toml')
makedepends=('python-setuptools-scm')
provides=('python-jaraco-text'
          'python-jaraco-functools'
          'python-jaraco-collections'
          'python-jaraco-classes'
          'python-jaraco-stream'
          'python-jaraco-logging'
          'python-jaraco-itertools')

# These should not exist and do not deserve so many packages.
# Furthermore, try to make a replacement for python-irc so we can nuke it all.
# And now cherrypy depends on this too :-(
_text=3.6.0
_functools=3.5.0
_collections=3.4.0
_classes=3.2.1
_stream=3.0.3
_logging=3.1.0
_itertools=6.0.3
_pyhost="https://files.pythonhosted.org/packages/source"

source=("$_pyhost/j/jaraco.text/jaraco.text-$_text.tar.gz"
        "$_pyhost/j/jaraco.functools/jaraco.functools-$_functools.tar.gz"
        "$_pyhost/j/jaraco.collections/jaraco.collections-$_collections.tar.gz"
        "$_pyhost/j/jaraco.classes/jaraco.classes-$_classes.tar.gz"
        "$_pyhost/j/jaraco.stream/jaraco.stream-$_stream.tar.gz"
        "$_pyhost/j/jaraco.logging/jaraco.logging-$_logging.tar.gz"
        "$_pyhost/j/jaraco.itertools/jaraco.itertools-$_itertools.tar.gz")
sha512sums=('74e6f199372b62ee767fd5f5829ab636d6d87c38998d93cb4ed7dddbe53db27e5c3caf45f36f7b5690b3c12367df0c93702fac59ee99ffcb03f53e70b8b18f96'
            '7257f41506f90ca2fda1bd12fed58fa4d4ff5738547f8cdae8ccad1901e0c818afb701dd8e9050fc77ca1271bc2466d5a542990138bc3db2cf9b04d12850844c'
            '8e427f98dbcfad3f9042d1f080b2bf8012fdaaf084e230f9b46ff078623b047c2d9dab41ed0303df5dd070abce2a25289281113a97e8ab8d819048d30b8d314a'
            '4c85cfdd1da88de01596867365147be6cadd7e1d230798683236fdfde579747e3309aac95ae4f3ef86c91bb4493f3096c4ecc14e3db764ef308aab41ead88e23'
            'fe50c21b26e734c9c2d0b7cf8a1c8cc8bab80417adf073a4ae213ba4067aa33e2f6c8886aac996971d2f98aacd295d9999aa3ac7dba24dd952cc27d15bef22e2'
            'ce9a9cbad6d6bb448615eac986d361f9c6ff477b1209e85433f12a49055a9e30125cd49bebcf915a08b8efe0732439a8e1d336e63f6454369b3e846a9a1383ba'
            '5ceb11c6a5f1c88fa6191f1153b5450614d9fc87ae08acbe4a417d8bf6089f5dae747b1cee95a29763ffc595c291ab02198cf94b48eca34ce766735c6dc44e30')

prepare() {
  # remove inflect dep
  cd "$srcdir/jaraco.itertools-$_itertools"
  sed -i "s/'inflect',//" setup.py
  sed -i "s/inflect//"    jaraco.itertools.egg-info/requires.txt
  sed -i "s/inflect//"    setup.cfg
  sed -i 's/import inflect//' jaraco/itertools.py
  sed -i 's/ord = .*$/ord = str(self.sliceArgs[2])+"th"/' jaraco/itertools.py
}

package() {
  cd "$srcdir/jaraco.text-$_text"
  python3 setup.py install --root="$pkgdir/" --optimize=0
  cd "$srcdir/jaraco.functools-$_functools"
  python3 setup.py install --root="$pkgdir/" --optimize=0
  cd "$srcdir/jaraco.collections-$_collections"
  python3 setup.py install --root="$pkgdir/" --optimize=0
  cd "$srcdir/jaraco.classes-$_classes"
  python3 setup.py install --root="$pkgdir/" --optimize=0
  cd "$srcdir/jaraco.stream-$_stream"
  python3 setup.py install --root="$pkgdir/" --optimize=0
  cd "$srcdir/jaraco.logging-$_logging"
  python3 setup.py install --root="$pkgdir/" --optimize=0
  cd "$srcdir/jaraco.itertools-$_itertools"
  python3 setup.py install --root="$pkgdir/" --optimize=0

  #rm "$pkgdir/usr/bin/calc-prorate"
  #rmdir "$pkgdir/usr/bin"
  # None of these packages have a license file?  Fix later.
  #install -Dm644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

# vim:set ts=2 sw=2 et:
